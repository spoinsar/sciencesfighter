\echo
\echo suppression des tables existante s il elles existe
\echo 

DROP TABLE IF EXISTS Choix_Joueur;
DROP TABLE IF EXISTS Comporte;
DROP TABLE IF EXISTS Duels;
DROP TABLE IF EXISTS Players;
DROP TABLE IF EXISTS Choix;
DROP TABLE IF EXISTS Questions;


\echo
\echo creation des tables
\echo

CREATE TABLE Questions(
    Id SERIAL PRIMARY KEY,
    Nom VARCHAR NOT NULL UNIQUE,
    ThemeLyc VARCHAR,
    ThemeLic VARCHAR,
    Level INTEGER,
    Cprygt VARCHAR,
    Enonce VARCHAR,
    GlobalExplication VARCHAR,
    Type VARCHAR,
    CHECK (Level >=0 AND Level <=5),
    CHECK (TYPE = 'QCM' OR TYPE = 'QCU')
);

CREATE TABLE Choix(
    ID SERIAL PRIMARY KEY,
    Nom_Question VARCHAR ,
    N_Choix INTEGER,
    Label VARCHAR,
    Solution BOOLEAN,
    Explication VARCHAR,
    FOREIGN KEY(Nom_Question) REFERENCES Questions(Nom)  
);  

CREATE TABLE Players (
    Login VARCHAR PRIMARY KEY,
    Pswd VARCHAR NOT NULL,
    Score INTEGER,
    Date_inscr TIMESTAMP WITHOUT TIME ZONE,
    Date_co TIMESTAMP WITHOUT TIME ZONE
);

CREATE TABLE Duels (

    Id SERIAL PRIMARY KEY,
    Archive BOOLEAN,
    Date_debut DATE,
    Date_archive DATE,
    Points_j1 INTEGER,
    Points_J2 INTEGER,
    Joueur1 VARCHAR ,
    Joueur2 VARCHAR,
    Nbr_questions INTEGER,
    calcul_points BOOLEAN,
    FOREIGN KEY(Joueur1) REFERENCES Players(Login), 
    FOREIGN KEY(Joueur2) REFERENCES Players(Login)
);

CREATE TABLE Comporte (
    Id_duel INTEGER,
    Id_question INTEGER,
    FOREIGN KEY(Id_duel) REFERENCES Duels(Id),
    FOREIGN KEY(Id_question) REFERENCES Questions(Id)
);

CREATE TABLE Choix_Joueur (

    joueur VARCHAR,
    Id_duel INTEGER,
    Id_Choix INTEGER,
    PRIMARY KEY(joueur,Id_duel,Id_Choix),
    FOREIGN KEY(joueur) REFERENCES Players(login),
    FOREIGN KEY(Id_duel) REFERENCES Duels(Id),
    FOREIGN KEY(Id_choix) REFERENCES Choix(Id)    
);

\echo
\echo insertion des jeux de test 
\echo

INSERT INTO Questions(Nom,ThemeLyc,ThemeLic,level,Cprygt,Enonce,GlobalExplication,Type) VALUES (
'Maths/POSmath/9083',
'Fonctions',
'Fonctions',
3,
'<a href="http://univ-angers.fr">univ-angers.fr</a>',
'On considère la fonction $f:x\mapsto x^3+x$.$f$ est :',
'$f$ est dérivable, de dérivée $f''(x)=3x^2+1$ strictement positive. $f$ est donc strictement positive sur $\mathbb{R}$ et seule la réponse 2) est correcte.', 
'QCU'
);

INSERT INTO Choix(Nom_Question,N_Choix,Label,Solution,Explication) VALUES (
'Maths/POSmath/9083',
1,
'décroissante sur $\mathbb{R}$',
'false',
'' );
INSERT INTO Choix(Nom_Question,N_Choix,Label,Solution,Explication) VALUES (
'Maths/POSmath/9083',
2,
'croissante sur $\mathbb{R}$',
'true',
'' );
INSERT INTO Choix(Nom_Question,N_Choix,Label,Solution,Explication) VALUES (
'Maths/POSmath/9083',
3,
'croissante sur $]-\infty,0]$ et décroissante sur $[0,+\infty[$',
'false',
'' );
INSERT INTO Choix(Nom_Question,N_Choix,Label,Solution,Explication) VALUES (
'Maths/POSmath/9083',
4,
'décroissante sur $]-\infty,0]$ et croissante sur $[0,+\infty[$',
'false',
'' );

INSERT INTO Questions(Nom,ThemeLyc,ThemeLic,level,Cprygt,Enonce,GlobalExplication,Type) VALUES (
'Maths/POSmath/9316',
'Sens de variation d''une suite numérique',
'Suites',
3,
'<a href=''http://unistra.fr''>unistra.fr</a>',
'Soit la suite ($u_{n})_{n \geq 0}$ définie par $u_{0}=1$ et la relation $u_{n+1}=\frac{1}{2}\left(u_{n}-1\right)$',
'',
'QCM'
);

INSERT INTO Choix(Nom_Question,N_Choix,Label,Solution,Explication) VALUES (
'Maths/POSmath/9316',
1,
'Pour tout $n\geq 0$,$u_{n}=\frac{-(n-1)(n+2)}{2^{n+1}}$',
'FALSE',
'Cette formule s''avère vraie pour $n=0,1,2,$ mais elle mise en défaut pour $n=3$ : en effet, on a $u_1=0$,$u_2=-\frac{1}{2}$,$u_3=\frac{1}{2}\left(-\frac{1}{2}-1\right)=-\frac{3}{4}$, alors que $\frac{-(3-1)(3+2)}{2^{3+1}}=-\frac{5}{8}$.'
);

INSERT INTO Choix(Nom_Question,N_Choix,Label,Solution,Explication) VALUES (
'Maths/POSmath/9316',
2,
'La suite est décroissante et bornée par -1 et 1',
'TRUE',
'Montrons tout d''abord par récurrence sur $n\geq 0$ que $-1\leq u_n\leq 1$: L''initialisation est vraie, pour l''hérédité, on suppose que $-1\leq u_n\leq 1$, on a donc $\frac{1}{2}\left(-1-1\right)\leq u_{n+1}\leq\frac{1}{2}\left(1-1\right)$, et donc$-1\leq u_{n+1} \leq 1$, ce qui achève la récurrence. Pour $n\geq0$,$ u_{n+1}-u_n=\frac{1}{2}(u_n+1)$. Par ce qui précède,$u_n+1\geq 0$, donc $u_{n+1}-u_n\leq 0$.'
);

INSERT INTO Choix(Nom_Question,N_Choix,Label,Solution,Explication) VALUES (
'Maths/POSmath/9316',
3,
'La suite n''est pas convergente',
'FALSE',
'La suite est décroissante bornée (voir l''explication de la réponse 2) donc converge.'
);

INSERT INTO Choix(Nom_Question,N_Choix,Label,Solution,Explication) VALUES (
'Maths/POSmath/9316',
4,
'La suite est convergente et sa limite est 0',
'FALSE',
'Si u_n tend vers zéro, $u_{n+1}=\frac{1}{2}\left(u_{n}-1\right)$ tend vers $\frac{1}{2}\left(0-1\right)=-\frac{1}{2}$. C''est impossible car si $(u_n)$ converge $u_n$ et $u_{n+1}$ ont la même limite. Plus généralement, $(u_n)$ étant définie par $u_0$ et $u_{n+1}=f(u_n)$, avec $f$ continue (ici $f :x\mapsto\frac{1}{2}\left(x-1\right)$), si $(u_n)$ converge vers un réel $\ell$, alors $f(\ell)=\ell$. Ici, on a $f(0)\neq 0$, ce qui prouve qu''on n''a pas "$(u_n)$ converge vers 0".'
);

INSERT INTO Questions(Nom,ThemeLyc,ThemeLic,level,Cprygt,Enonce,GlobalExplication,Type) VALUES (
'Chimie/POSchim/8981',
'énantiométrie,mélange racémique,diastéréoisomérie',
'chimie organique',
NULL,
'<a href=''http://univ-lille1.fr''>univ-lille1.fr</a>',
'<strong>Enantiomères, mélange racémique</strong>',
'<strong>Compétence(s).  Définir des énantiomères, un mélange racémique. Reconnaître deux énantiomères.</strong>
Des énantiomères sont des molécules chirales et images l''une de l''autre dans un miroir plan.
Un mélange racémique est un mélange équimolaire d''un couple d''énantiomères.',
'QCM'
);

INSERT INTO Choix(Nom_Question,N_Choix,Label,Solution,Explication) VALUES (
'Chimie/POSchim/8981',
1,
'Des énantiomères sont des molécules achirales et images l''une de l''autre dans un miroir plan.',
'FALSE',
''
);
INSERT INTO Choix(Nom_Question,N_Choix,Label,Solution,Explication) VALUES (
'Chimie/POSchim/8981',
2,
'Un mélange équimolaire d''un couple d''énantiomères est un mélange racémique.',
'TRUE',
''
);
INSERT INTO Choix(Nom_Question,N_Choix,Label,Solution,Explication) VALUES (
'Chimie/POSchim/8981',
3,
'Des énantiomères sont des molécules chirales et images l''une de l''autre dans un miroir plan.',
'FALSE',
''
);
INSERT INTO Choix(Nom_Question,N_Choix,Label,Solution,Explication) VALUES (
'Chimie/POSchim/8981',
4,
'<img src="/Ressources/Chimie/POSchim/images/tle_q25_gglyceraldehyde_enantiomeres.png" alt="tle_q25_gglyceraldehyde_enantiomeres">',
'TRUE',
'Ce sont des molécules chirales (le C central est asymétrique) et elles sont images l''une de l''autre dans un miroir plan.'
);

INSERT INTO Questions(Nom,ThemeLyc,ThemeLic,level,Cprygt,Enonce,GlobalExplication,Type) VALUES (
'Chimie/POSchim/8987',
'catégorie de réaction: substitution,addition,élimination',
'Cimie organique',
1,
'<a href=''http://unice.fr''>unice.fr</a>',
'Considérons la réaction :
<img src="/Ressources/Chimie/POSchim/images/image15.png"
De quel type de réaction s''agit-il ?',
'Ici, le groupement $Cl_-$ a été remplacé par un groupement $OH_-$. Nous avons à faire à une réaction de substitution nucléophile, certainement de type Sn2.',
'QCM'
);

INSERT INTO Choix(Nom_Question,N_Choix,Label,Solution,Explication) VALUES (
'Chimie/POSchim/8987',
1,
'Une élimination.',
'FALSE',
''
);

INSERT INTO Choix(Nom_Question,N_Choix,Label,Solution,Explication) VALUES (
'Chimie/POSchim/8987',
2,
'Une addition.',
'FALSE',
''
);


INSERT INTO Choix(Nom_Question,N_Choix,Label,Solution,Explication) VALUES (
'Chimie/POSchim/8987',
3,
'Une substitution nucléophile..',
'TRUE',
''
);

INSERT INTO Choix(Nom_Question,N_Choix,Label,Solution,Explication) VALUES (
'Chimie/POSchim/8987',
4,
'Une oxydation.',
'FALSE',
''
);


INSERT INTO Choix(Nom_Question,N_Choix,Label,Solution,Explication) VALUES (
'Chimie/POSchim/8987',
5,
'Toutes les propositions sont fausses.',
'FALSE',
''
);

INSERT INTO players (login, pswd, score, date_inscr, date_co) VALUES (
'fuloy',
'$2y$11$1562682b941cb76dfa2b4OpkUpiXKtdJiunmPbImYn9hdZXKdtjCi',
0,
'2018-02-13 12:34:59',
'2018-02-13 12:35:10'
);

INSERT INTO players (login, pswd, score, date_inscr, date_co) VALUES (
'tempest',
'$2y$11$6ad8536215968eb1f3494ugA4sH35UYWM3Tjqn.cOO1GKfvJdw2nm',
0,
'2018-02-13 12:35:31',
'2018-02-13 15:58:35'
);

INSERT INTO players (login, pswd, score, date_inscr, date_co) VALUES (
'tardi',
'$2y$11$db8b9677c3d2085a21a3duTJstTi1/DLYuVPb/cSrrXe6Qqb8hL3G',
0,
'2018-02-13 12:05:23',
'2018-02-13 13:10:38'
);

INSERT INTO players (login, pswd, score, date_inscr, date_co) VALUES (
'tadi',
'$2y$11$576e6b3da6fa23087c2f4uLvw0uYJDlB..lkVupIo9DeXZ994OWeG',
0,
'2018-02-13 13:37:03',
'2018-02-13 13:59:50'
);

INSERT INTO players (login, pswd, score, date_inscr, date_co) VALUES (
'test',
'$2y$11$098f6bcd4621d373cade4e2IoEQ/BiFFUTygtuHSLAAUSHgPsiEgK',
1000,
'2018-02-13 11:45:50',
'2018-02-13 15:47:24'
);



\echo
\echo ajout des droit pour l utilisateur cap
\echo

GRANT SELECT,UPDATE,INSERT on Comporte,Duels,Players,Choix,Questions,Choix_Joueur TO cap ;
GRANT USAGE,SELECT on ALL SEQUENCES IN SCHEMA public TO cap ;

\echo
\echo fin du script






 
