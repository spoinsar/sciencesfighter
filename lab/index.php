<?php

require "entete.php";
    if(isset($_SESSION["name"])){
    
            Flight::route('/', function(){
                echo "hello world!";
            });
            
                
            Flight::route('GET /count', function(){
                include "compteur/compte.php";
            });
                   
            //joueur courant et panel de controle
            Flight::route('GET /'.$_SESSION["name"], function(){
                echo "<p>voici vos infos  ".$_SESSION["name"]."</p>";
                
                
                include_once "fonction/infoPlayer.php";
                $duel = duel_defense($_SESSION["name"]);
                
                //si le joueur est attaque par quelqu'un d'autre si il n'a pas a ce defendre le bouton n'apparaitra pas
                if($duel){
                    
                    echo "<form class='form-defense' method='POST' action='/lab/".$_SESSION["name"]."/".$duel["joueur1"]."/games/". $duel["id"] ."'>";
                    echo    "<input type='submit' value='Se Defendre contre " . $duel["joueur1"] . "'>";                    
                    echo "</form>";
                }
                echo getTabDuelsPlayer($_SESSION["name"]);
                
                echo "<p><a href='/lab/".$_SESSION["name"]."/players'>liste des joueurs</a></p>";
            });
            
            //liste des joueurs
            Flight::route('GET /'.$_SESSION["name"].'/players', function(){
                include_once "fonction/players.php";
            });
            
            //infos sur le joueur @id
            Flight::route('GET /'.$_SESSION["name"].'/@id:[A-z0-9]*', function($id){
                echo "vous êtes sur le compte de : $id";
                echo "<p><a href='/lab/" . $_SESSION["name"] . "/$id/games'>champ de bataille de $id</a></p>";
            });
    
            //les infos des parties du joueur
            Flight::route('GET /'.$_SESSION["name"].'/@id:[A-z0-9]*/games', function($id){
                //quant le joueur est en attaque ou en defense et que l'on veux le renvoyer sur son attaque ou sa defense (resou le probleme du post)
                if(isset($_SESSION["type_duel"])){
                    if( $_SESSION["type_duel"] == "attaque") 
                        //on vas sur le reste des question (cela resout le probleme de la redirection sans passer par la page en post)
                        include_once "fonction/attaque.php";
                    else 
                        include_once "fonction/defense.php";
                }
                else{
                    //
                    echo "<p>vous êtes sur le champs de bataille de : $id<p>";
                    include_once "fonction/infoPlayer.php";
                    echo getTabDuelsPlayer($id);
                    echo "<p><a href='/lab/".$_SESSION["name"]."/players'>liste des joueurs</a></p>";
                }
            });
            
            //commencer un nouveaux duel , on repond au question
            Flight::route('POST /'.$_SESSION["name"].'/@id:[A-z0-9]*/games', function($id){
                include "fonction/attaque.php";
            });
            
            //on lance la defense n'@num pour le player courant
            Flight::route('POST /'.$_SESSION["name"].'/@id:[A-z0-9]*/games/@num:[0-9]*', function($id,$num){
                include_once "fonction/defense.php";
            });
            
            //on envoie les reponces du joueur pour une question
            Flight::route('POST /'.$_SESSION["name"].'/@id:[A-z0-9]*/games/@num:[0-9]*/choices/@ques:[0-9]*', function($id,$num,$ques){
                include_once "fonction/choices.php";
            });
    
    
                
            Flight::start();
    }


require "foot.php";


?>
