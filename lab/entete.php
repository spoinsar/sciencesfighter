<?php 
    session_start();
    ob_start();
    require_once 'flight/Flight.php';
    $GLOBALS["nbr_questions"] = 3;
    

?>
<!DOCTYPE html>
<html>
    <head>
        <meta charset="UTF-8"/>
        <title>science wars</title>
        
        <script type="text/javascript" src="/lab/js/jquery.js"></script>
        
        <link rel="stylesheet" type="text/css" href="/lab/js/semantic/semantic.min.css">
        <link rel="stylesheet" type="text/css" href="/lab/css/players.css">
        
        <script type="text/javascript" src="/lab/js/semantic/semantic.min.js"></script>
        
        <script type="text/x-mathjax-config">
            MathJax.Hub.Config({tex2jax: {inlineMath: [['$','$'], ['\\(','\\)']]}});
        </script>
        <script type="text/javascript" async
          src="/lab/js/MathJax/MathJax.js?config=TeX-MML-AM_CHTML">
        </script>
       
        <?php
        
        if(isset($_SESSION["name"])){
    ?>
            <script type="text/javascript">
            name = "<?php echo $_SESSION["name"];?>" ;
		setInterval("envoie_rappel()",5000)
		
		function envoie_rappel(){
    		console.log("envoi d'un message de rappel");
    		console.log(name);
    		if(name != null){
    			$.post("/lab/fonction/user.php",{name:name},function(){});	
    		}
		}
		envoie_rappel();
        </script>
        
             
    </head>
    <body>
        <?php 
        }
        else{
		?>
		<p> Forumlaire d'inscription </p>
		
		<form method="post" action="/lab/fonction/inscription.php" class="ui form">
			<div class="field">
				<label for="pseudo">Votre pseudo</label> <input type="text" placeholder="Pseudo" name="pseudo" id="pseudo"/>
    		</div>
    		<div class="field">
    			<label for="password">Password</label> <input type="password" placeholder="Password" name="password" id="password"/>
    		</div>
    		<button class="ui button" type="submit" id="bouton-connexion">Connexion</button>
    	</form>
    

   		<?php
        }
   		if(isset($_SESSION["name"])){
                echo "<p>vous êtes <a href='/lab/". $_SESSION["name"] ."'> " . $_SESSION["name"] . "</a><p>" ;
                echo "<form method='get' action='/lab/fonction/deconnexion.php' class='ui form'>";
                echo "<button class='ui button' type='submit' id='bouton-deconnexion'>Deconnexion</button>";
                echo "</form>";
   		}
   		//session_destroy();
?>