<?php

require_once $_SERVER["DOCUMENT_ROOT"]."/lab/connexion/connexion.php";
$db = $GLOBALS["db"];


//penser a optimiser le code pour l'attaque et la defense il y a juste le renvoie qui change
if(isset($_SESSION["type_duel"])){
    
    //on stocke dans la bdd les reponses pour une defense (moins de boulot) 
    if($_SESSION["type_duel"] == "defense"){
        
        $id_duel = $_SESSION["id_duel"]["id"];
        $id_question = $_SESSION["id_question"];
        
        $number_choice = get_number_choice($id_question);
        
        $catch_choice_user = true ;
        
        if($catch_choice_user){
            
            
            $request_add_choice = "" ;
            
            
            
            for($i = 1 ; $i <= $number_choice ; $i++){
                
                if( isset($_POST["reponse".$i]) ){
                    echo "<p>test : </p>";
                    if($_POST["reponse".$i] == "on"){
                        try{
                            echo " joueur : ".$_SESSION["name"]; 
                            echo " id_duel : ".$_SESSION["id_duel"]["id"]; 
                            echo " id_choix : ".$i; 
                            
                            $request_add_choice = "INSERT INTO Choix_joueur(joueur,id_duel,id_choix) values (:joueur,:id_duel,:id_choix)";
                            $request_prepare = $db->prepare($request_add_choice);
                            
                            
                            
                            //remplir le $SESSION[@
                            $request_prepare->execute(Array(":joueur"=>$_SESSION["name"],":id_duel"=> $_SESSION["id_duel"]["id"],":id_choix"=>$_POST["reponse".$i] ));
                            
                        }
                        catch(Exception $e){
                            echo $e;
                        }
                        
                    }
                }
            }
            
            
            
            /*echo "<form method='POST' action='/lab/" . $_SESSION["attaquant"] . "/" . $_SESSION["defenseur"] . "/games'>";
            
            echo "<button type='submit' value='submit'> retourner aux questions </button>" ;
            
            echo "</form>" ;*/
            
            
            //on a besoin du defenseur et de l'attaquant lors de se duel
            
            $request = "select joueur1,joueur2 from duels where id = :id";
            $request_prepare = $db->prepare($request);
            $request_prepare->execute(Array(":id"=>$id_duel));
            $joueurs = $request_prepare->fetch(PDO::FETCH_ASSOC);
            
            //dans le acs de la defense c'est le deuxieme joueur dans la table duel qui est defenseur et le premier joueur est attaquant
           header("Location: /lab/".$joueurs["joueur2"]."/".$joueurs["joueur1"]."/games");
           die();
            
        }
        
    }
    
    //on stocke dans la bdd les reponses pour une attaque
    if($_SESSION["type_duel"] == "attaque"){
        //$_SESSION["questions"] contient l'id des question qui on ete pose pour le duel la derniere correspond a celle qui a ete posez et $_SESSION["id_duel"] correspond a l'id du duel en cours
        if(isset($_SESSION["questions"]) && isset($_SESSION["id_duel"])){
            
            
            
            
            $id_question = $_SESSION["questions"][count($_SESSION["questions"]) -1 ];
            $number_choice = get_number_choice($id_question);
            
            //pouvoir modifier ceci s'il y a un probleme dans la gestion de la reception du choix de l'utilisateur
            $catch_choice_user = true ;
            
            /*for($i = 1 ; $i <= $number_choice ; $i++){
                if( !isset($_POST["reponse".$i]) ){
                    $catch_choice_user = false;
                }
                
            }*/
            
            if($catch_choice_user){
                
                $request_add_choice = "" ;
                
                
                
                for($i = 1 ; $i <= $number_choice ; $i++){
                    if( isset($_POST["reponse".$i]) ){
                    
                        if($_POST["reponse".$i] == "on"){
                            try{
                                $request_add_choice = "INSERT INTO Choix_joueur(joueur,id_duel,id_choix) values (:joueur,:id_duel,:id_choix)";
                                $request_prepare = $db->prepare($request_add_choice);
                                $request_prepare->execute(Array(":joueur"=>$_SESSION["name"],":id_duel"=>$_SESSION["id_duel"],":id_choix"=>$_SESSION["id_choix"][$i-1] ));    
                            }
                            catch(Exception $e){
                                echo $e;
                            }
                        
                        }
                    }
                }
                
        
                
                /*echo "<form method='POST' action='/lab/" . $_SESSION["attaquant"] . "/" . $_SESSION["defenseur"] . "/games'>";
                
                echo "<button type='submit' value='submit'> retourner aux questions </button>" ;
                
                echo "</form>" ;*/
                header("Location: /lab/".$_SESSION["attaquant"]."/".$_SESSION["defenseur"]."/games");
                die();
               
            }
            else{
                echo "quelque chose est mal passe les réponses non pas bien été communiqué" ;
            }    
        }
    }
}
//retourne un int
function get_number_choice($id_question){
    
    try{
        $db = $GLOBALS["db"];
        
        //on recupere le nombre de choix qu'a la question
        $request_number_choice = "select count(*) AS number_choices FROM questions,choix WHERE questions.id = :id AND questions.nom = choix.nom_question " ;
        $request_prepare = $db->prepare($request_number_choice);
        $request_prepare->execute(Array(":id"=>$id_question));
        
        $number_choice = $request_prepare->fetch(PDO::FETCH_ASSOC);
    }
    catch(Exception $e){
        echo $e;
    }
    
    return $number_choice["number_choices"];
}
header("Location: /lab/");
die();