<?php
echo "defense en cours";

require_once $_SERVER["DOCUMENT_ROOT"]."/lab/connexion/connexion.php";



include_once $_SERVER["DOCUMENT_ROOT"]."/lab/fonction/infoPlayer.php";


if(isset($_SESSION["name"])){
    $db = $GLOBALS["db"];
    
    if(!isset($_SESSION["id_duel"])){
        //on prend l'id du duel (si l'utilisateur se retrouve ici par des manipulation douteuse il sera quand meme forcer de faire la defense qu'on veut lui faire faire)
        $_SESSION["id_duel"] = duel_defense($_SESSION["name"]);
        
        //on fait en sorte que le duel soit archive et donc plus personne ne peux rien repondre si l'utilisateur quitte en cours de réponse il ne pourra pas refaire la defense elle sera compter comme fini
        $request_set_archive = "update duels set archive = true where id = :id" ;
        $request_prepare = $db->prepare($request_set_archive);
        $request_prepare->execute(Array(":id"=>$_SESSION["id_duel"]["id"]));
        
        //on met a jour la date d'archive
        $date = new DateTime();
        $request_set_date_archive ="update duels set date_archive = :date where id = :id";
        $request_prepare = $db->prepare($request_set_date_archive);
        $request_prepare->execute(Array(":date"=>$date->format("Y-m-d H:i:s"),":id"=>$_SESSION["id_duel"]["id"]));
    }
    
    if(!isset($_SESSION["type_duel"])){
        //on definie le type du duel pour biens diffrencier dans choices.php si l'uitilisateur est en defense ou en attaque
        $_SESSION["type_duel"] = "defense";
    }
    
    
    //on verifie si l'utilisateur etait deja en defense ou pas
    if(isset($_SESSION["id_questions"])){
        
        //on verifie s'il reste des questions s'il n'en reste pas on force l'utilisateur a quitter la page et on detruit les variables de session creer au sein de la page
        if(!isset($_SESSION["id_questions"][0])){
            
            
            
            unset($_SESSION["id_questions"]);
            unset($_SESSION["type_duel"]);
            unset($_SESSION["id_duel"]);
            unset($_SESSION["id_question"]);
            
            
            
            header("Location: /lab/".$_SESSION["name"]);
            die();
        }
    }
    else{
        $_SESSION["id_questions"] = liste_question($_SESSION["id_duel"]["id"])->fetchAll(PDO::FETCH_ASSOC);
    }
    
    
    
    $question = get_question($_SESSION["id_questions"][0]['id_question']);
    
    
    
    $time_awnser_limit = 60 ;
    
    
    //on cherche les choix de la question
    $request_choix_question = "SELECT * from choix where nom_question = :nom_question " ;
    $request_prepare = $db->prepare($request_choix_question);
    $request_prepare->execute(Array(":nom_question"=>$question["nom"]));
    
    echo "<p id='time'>voici la question n'" . $_SESSION["id_questions"][0]['id_question'] . " tu a ".$time_awnser_limit." secondes pour y repondre : </p>";
    
    echo "<p> question d'id : " . $question["id"] . " voici l'énoncé : " . $question["enonce"] . "</p>";
    
    //a revoir
    echo "<form id='envoie-reponse' method='post' action='/lab/" . $_SESSION["name"] . "/" . $defenseur . "/games/" . $_SESSION["id_duel"]["id"] . "/choices/" . $question["id"] . "'>" ;
    
    
    while($choix = $request_prepare->fetch(PDO::FETCH_ASSOC)){
        
        echo "<input type='checkbox' name='reponse" . $choix["n_choix"] . "' >" . $choix["label"] . "</br>" ;
        
    }
    
    
    echo "<button type='submit' value='submit' > envoyer la r&eacute;ponse</button>";
    echo "</form>";
    
    //envoie du script qui vas gerer le timer si il est changer ça n'influencera rien sur la durée réel de la question
    ?>
            	<script>
                	var timer = <?php echo $time_awnser_limit;?> ;
                	var message = "voici la question n'" + <?php echo $question["id"];?> + " tu a " + timer + " secondes pour y repondre : ";

                	function change_timer(){
                		timer--;
						var message = "voici la question n'" + <?php echo $question["id"];?> + " tu a " + timer + " secondes pour y repondre : ";
						$("#time").text(message);
                    }
                    setInterval("change_timer()",1000)

            	</script>
            <?php 
    
    //on stocke l'id de la question pour la retrouver lors de la soumission des choix
    $_SESSION["id_question"] = $_SESSION["id_questions"][0]["id_question"];
    
    //on coupe la liste des question pour enlever la premiere
    $_SESSION["id_questions"] = array_slice($_SESSION["id_questions"],1);
    
    
    header("Refresh: ".$time_awnser_limit);
    
    
}

/* tableau des question qui compose l'attaque :
array (size=3)
0 =>
array (size=1)
'id_question' => int 4
1 =>
array (size=1)
'id_question' => int 2
2 =>
array (size=1)
'id_question' => int 3
*/
//on recupere la liste des question pour un duel donner
function liste_question($id_duel){
    $db = $GLOBALS["db"];
    $query = "select comporte.id_question from duels,comporte where duels.joueur2 = :player AND duels.id=comporte.id_duel AND duels.id = :duel_id";
    $query_prepare = $db->prepare($query);
    $query_prepare->execute(Array(":player"=>$_SESSION["name"],":duel_id"=>$id_duel));
    
    //dans $query_prepare on a la reponse de la base quand on lui demande les question qui compose la defense qui correspond a $id_duel
    return $query_prepare;
    
}

//on retourne la question dans un tableau associatif (PDO::FETCH_ASSOC)
function get_question($id_question){
    $db = $GLOBALS["db"];
    $query = "select * from questions where id = :id";
    $query_prepare = $db->prepare($query);
    $query_prepare->execute(Array(":id"=>$id_question));
    
    $question = $query_prepare->fetch(PDO::FETCH_ASSOC);
    return $question;
}