<?php

session_start();
require_once $_SERVER["DOCUMENT_ROOT"]."/lab/connexion/connexion.php";
$db = $GLOBALS["db"];

if(isset($_POST["pseudo"]) && isset($_POST["password"])){
    
    if( trim($_POST["pseudo"]) != "" || trim($_POST["password"]) != "" ) {
        
        $pseudo = htmlspecialchars($_POST["pseudo"]);
        $password = htmlspecialchars($_POST["password"]);
        
        
        
        $requete = "SELECT * FROM players WHERE login = :pseudo";
        
        try{
            //on cherche a savoir si le joueur existe
            $requete_prepare = $db->prepare($requete);
            $requete_prepare->execute(array(":pseudo"=>$pseudo));
            
            //si il existe on verifie le mot de passe
            if($player = $requete_prepare->fetch(PDO::FETCH_ASSOC)){
                
                //check password
                if(password_verify($password,$player["pswd"])){
                    
                    //set the $session name on initialise la $_SESSION["name"] l'utilisateur est bien connnecte 
                    $_SESSION["name"] = $player["login"];
                    
                    header("Location: /lab/".$_SESSION["name"]);
                    die();
                }
             //la mot de passe n'est pas correcte  
             else{
                    echo "<p>erreur de password</p>";
                    echo "<a href='/lab/'>retour a la page d'incscription</a>";
             }
            }
            //si le joueur n'existe pas
            else{
                
                //on cree le grain de sel a partir du pseudo de l'utilisateur 
                $options = [
                    'cost' => 11,
                    'salt' => hash("md5",$pseudo),
                ];
                
                //on hash le mot de passe avec les otpions pour le grain de sel
                $password = password_hash($password,PASSWORD_BCRYPT,$options);
                
                $date = new DateTime();
                
                //on envoie la requete du nouvelle utilisateur
                $requete = "INSERT INTO Players(login,Pswd,Score,Date_inscr,Date_co) VALUES (:login,:password,0,:date,:date)";
                $requete_prepare = $db->prepare($requete);
                $requete_prepare->execute(array(":login"=>$pseudo,":password"=>$password,":date"=>$date->format("Y-m-d H:i:s")));
                echo "<p>compte crée avec succès<p>";
                $_SESSION["name"] = $pseudo;
                echo "<a href='/lab/" . $player["login"] . "'>votre compte</a>" ;
            }
        }
        catch(Exception $e){
            echo $e;
        }
    }
    else{
        connexion_impossible();
    }
}

else{
    connexion_impossible();    
}

function connexion_impossible(){
    echo "<p>connexion impossible</p>";
    echo "<a href='/lab/'>retour a la page d'incscription</a>";
}

