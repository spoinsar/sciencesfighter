<?php

session_start();

require_once $_SERVER["DOCUMENT_ROOT"]."/lab/connexion/connexion.php";

$nombre_question = $GLOBALS["nbr_questions"];



if( isset($_SESSION["attaquant"]) && isset($_SESSION["defenseur"])){
    
    $attaquant = htmlspecialchars($_SESSION["attaquant"]);
    $defenseur = htmlspecialchars($_SESSION["defenseur"]);
    
    echo "<p>reprise du duel</p>" ;
}



else if( (isset($_POST["attaquant"]) && isset($_POST["defenseur"])) ){
    
    $attaquant = htmlspecialchars($_POST["attaquant"]);
    $defenseur = htmlspecialchars($_POST["defenseur"]);
    echo "<p>nouvelle attaque</p>";
    
}
 
if(isset($attaquant) && isset($defenseur)){
    if($attaquant == $_SESSION["name"]){
        
        $db = $GLOBALS["db"];
        
        $_SESSION["attaquant"] = $attaquant ;
        $_SESSION["defenseur"] = $defenseur ;
        //on definie le type du duel pour biens diffrencier dans choices.php si l'uitilisateur est en defense ou en attaque
        $_SESSION["type_duel"] = "attaque";
        
        try{
            if(isset($_SESSION["num_question"])){
                if($_SESSION["num_question"] >= $nombre_question){
                    unset($_SESSION["attaquant"]);
                    unset($_SESSION["defenseur"]);
                    unset($_SESSION["num_question"]);
                    unset($_SESSION["questions"]);
                    unset($_SESSION["id_duel"]);
                    unset($_SESSION["id_choix"]);
                    unset($_SESSION["type_duel"]);
                    
                    header("Location: /lab/".$_SESSION["name"]);
                    die();
                }
            }
            
            if(isset($_SESSION["num_question"])){
                $_SESSION["num_question"]++;
            }
            else{
                $_SESSION["num_question"] = 1;
                
                $request_insert_duel = "INSERT INTO Duels (Archive,Date_debut,Joueur1,Joueur2,Nbr_questions,calcul_points) VALUES (FALSE,:DATE,:J1,:J2,$nombre_question,false) RETURNING id";
                $request_prepare = $db->prepare($request_insert_duel);
                
                $date = new DateTime();
                $request_prepare->execute(Array(":DATE"=>$date->format("Y-m-d H:i:s") ,":J1"=>$attaquant,":J2"=>$defenseur));
                //id of the duel for fill the table comporte
                $_SESSION["id_duel"] = $request_prepare->fetch(PDO::FETCH_ASSOC)["id"] ;
            }
            
            //on verifie si la variable qui gere les question deja poser existe 
            if(isset($_SESSION["questions"])){
                $request_question = "SELECT * FROM questions WHERE ";
                
                //on cherche dans la table toute les question et on eleve les question qui on deja ete posez
                for($i = 0 ; $i < sizeof($_SESSION["questions"]) ; $i++ ){
                    if($i == 0){
                        //not safe but this variable is only set by php
                        
                        $request_question = $request_question." id NOT IN (SELECT id FROM questions WHERE id = " . $_SESSION["questions"][$i] . ") ";
                    }
                    else{
                        $request_question = $request_question." AND id NOT IN (SELECT id FROM questions WHERE id = " . $_SESSION["questions"][$i] . ")" ; 
                    }
                }
                $request_question = $request_question . " ORDER BY RANDOM() LIMIT 1" ;
            }
            
            else{
                $_SESSION["questions"] = array();
                $request_question = "SELECT * FROM questions ORDER BY RANDOM() LIMIT 1";
            }

            $request_prepare = $db->prepare($request_question);
            $request_prepare->execute();
            $question = $request_prepare->fetch(PDO::FETCH_ASSOC);
            
            //limite de temps pour repondre, on pourra la modifier a volonte dans le futur
            $time_awnser_limit = 60 ;
            
            
            echo "<p id='time'>voici la question n'" . $_SESSION['num_question'] . " tu a ".$time_awnser_limit." secondes pour y repondre : </p>";
            
            $_SESSION["questions"][] = $question["id"];
            
            echo "<p> question d'id : " . $question["id"] . " voici l'énoncé : " . $question["enonce"] . "</p>";
            
            //on cherche les different choix de la question
            $request_choix_question = "SELECT * from choix where nom_question = '" . $question["nom"] ."'" ;
            $request_prepare = $db->prepare($request_choix_question);
            $request_prepare->execute();
            
            $_SESSION["id_choix"] = array();

            
            echo "<form id='envoie-reponse' method='post' action='/lab/" . $_SESSION["name"] . "/" . $defenseur . "/games/" . $_SESSION["id_duel"] . "/choices/" . $question["id"] . "'>" ;
            

            while($choix = $request_prepare->fetch(PDO::FETCH_ASSOC)){
                $_SESSION["id_choix"][] = $choix["id"];
                echo "<input type='checkbox' name='reponse" . $choix["n_choix"] . "' >" . $choix["label"] . "</br>" ;
                
            }
            
            
            echo "<button type='submit' value='submit' > envoyer la r&eacute;ponse</button>";
            echo "</form>";
            
            //envoie du script qui vas gerer le timer
            ?>
            	<script>
                	var timer = <?php echo $time_awnser_limit;?> ;
                	var message = "voici la question n'" + <?php echo $_SESSION['num_question'];?> + " tu a " + timer + " secondes pour y repondre : ";

                	function change_timer(){
                		timer--;
						var message = "voici la question n'" + <?php echo $_SESSION['num_question'];?> + " tu a " + timer + " secondes pour y repondre : ";
						$("#time").text(message);
                    }
                    setInterval("change_timer()",1000)

            	</script>
            <?php 
            //on remplit la table comporte avec la question que l'on viens de poser
            $request_comporte = "INSERT INTO Comporte(id_duel,id_question) VALUES (" . $_SESSION["id_duel"] . "," . $question["id"] . ")";
            $request_prepare = $db->prepare($request_comporte);
            $request_prepare->execute();
            //la page sera "refresh" au bout du temps laisse pour repondre
            header("Refresh: ".$time_awnser_limit);
        }catch(Exception $e){
            echo($e);
        }
    }
}


