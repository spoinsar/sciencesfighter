<?php

session_start();
require_once $_SERVER["DOCUMENT_ROOT"]."/lab/connexion/connexion.php";




//quand une requete par post est reçu
if(isset($_POST["name"])){
    $login = htmlspecialchars($_POST["name"]);
    //on verifie les compte (a faire avec un token)
    if($login == $_SESSION["name"]){
        //on renvoie la reponse a une requet jquery qui demande toute les x secondes si l'utilisateur est encore la
        return json_encode(array("login"=>maj_user_co($login)));
    }
}
    //on m'est a jour la date de connexion de l'utilisateur
    function maj_user_co($login){
        $db = $GLOBALS["db"];   
        $sql_request = "select login from players where login = :login" ;
        
        $date = new DateTime();
        try{
            $req_prepare = $db->prepare($sql_request);
            $req_prepare->execute(array(':login' => $login));
                
                //on m'est à jour le timestamp
                $sql_request = "UPDATE players SET date_co = :date WHERE login = :login";
                $req_prepare = $db->prepare($sql_request);
                $req_prepare->execute(array(":date" =>$date->format('Y-m-d H:i:s'),":login" =>$login));
                
        }
        catch (PDOException $e){
            echo $e->getMessage();
        }

        return $_SESSION["name"];
    }

if(isset($_GET["nbr_user"])){
    echo json_encode(array("nbr_user_co"=> nbr_user()));
}

function nbr_user(){
    $db = $GLOBALS["db"];
    
    
    //on compte tout les utilisateur connecte
    
    //on regarde 10 secondes en arriere
    $date_with_10_sec = new DateTime();
    $date_with_10_sec->sub(new DateInterval("PT10S"));
    
    $sql_request = "select count(*) AS nbr_connect FROM players WHERE date_co > :date_10_sec";
    $nbr_user = 0;
    
    try{
        
        $req_prepare = $db->prepare($sql_request);
        
        //on cherche tout les utilisateur qui sont connecte depuis les 10 derniere secondes
        $req_prepare->execute(array(":date_10_sec"=>$date_with_10_sec->format("Y-m-d H:i:s")));
        $res = $req_prepare->fetch(PDO::FETCH_ASSOC);
        $nbr_user = $res["nbr_connect"];
        
    }
    catch (PDOException $e){
        echo $e->getMessage();
    }
    return  $nbr_user;
}

function user_connected($name_user){
    $db = $GLOBALS["db"];
    $date_with_10_sec = new DateTime();
    $date_with_10_sec->sub(new DateInterval("PT10S"));
    
    //on regarde si le compte est connecte ou pas
    $sql_request = "select * FROM players WHERE login = :player_name AND date_co > :date_10_sec";;
    try{
        
        $req_prepare = $db->prepare($sql_request);
        $req_prepare->execute(array(":player_name"=>$name_user,":date_10_sec"=>$date_with_10_sec->format("Y-m-d H:i:s")));
        
        //on result and the user are co
        if($req_prepare->fetch(PDO::FETCH_ASSOC)){
            return true;
        }
        
    }catch(Exception $e){
        echo $e;
    }
    return false;
}

// a implemente il faudra juste mettre un temps entre deux attaque sur un utilisateur 
function attaquable($attaquant,$defendeur){
    if($attaquant == $defendeur){
        return false;
    }
    return true ;
}