
<?php 

    


    function getTabDuelsPlayer($joueur_courant){
        
        require_once $_SERVER["DOCUMENT_ROOT"]."/lab/connexion/connexion.php";
        $db = $GLOBALS["db"];
        
        $sql_request = "SELECT id,archive,date_debut,Points_j1,Points_j2,joueur1,joueur2,calcul_points FROM Duels where joueur1 = :attaquant or joueur2 = :attaquant ORDER BY date_debut desc";
        $request_prepare = $db->prepare($sql_request);
        $request_prepare->execute(Array(":attaquant"=>$joueur_courant));
        
        
        $tabl = "<h1>DUELS</h1>
                <table class='ui celled table'>
                	<thead>
                    	<tr>
                    		<th>ID</th>
                    		<th>type</th>
                    		<th>Joueur Adverse</th>
                        	<th>Date Début</th>
                            <th>Points J1</th>
                            <th>Points J2</th>
                            <th>Status</th>
                        </tr>
                </thead>
                <tbody>";
        while($duel = $request_prepare->fetch(PDO::FETCH_ASSOC)){
            
            //si la date depasse [inserer date ici] on considère que le joueur qui n'a pas répondue à l'agression a abandonné et donc perdra des points
            verification_date_abandon($duel);
            
            //si le duel est archivé et que des points non pas encore été attribué on fait le compte
            if(!$duel["calcul_points"] && $duel["archive"]){
                compte_point($duel);
            }
            
            $tabl .= "<tr>";
            $tabl .= "<td>" . $duel["id"] . "</td>";
            
            $tabl .= "<td>";
            
            $tabl .= type_duel($joueur_courant,$duel["joueur1"]);
            
            $tabl .= "</td>";
            $joueur_adverse = "";
            if(type_duel($joueur_courant,$duel["joueur1"]) == "attaque"){
                $joueur_adverse = $duel["joueur2"];
            }
            else{
                $joueur_adverse = $duel["joueur1"];
            }
            $tabl .= "<td> <a href='/lab/" . $_SESSION["name"] . "/" . $joueur_adverse . "'>" . $joueur_adverse . "</a></td>";
            
            
            $tabl .= "<td>" . $duel["date_debut"] . "</td>";
            $tabl .= "<td>" . $duel["points_j1"] . "</td>";
            $tabl .= "<td>" . $duel["points_j2"] . "</td>";
            $tabl .= "<td>".etat_duels($joueur_courant,$duel)."</td>";
            $tabl .= "</tr>";
        }
        
        
        $tabl .= "</tbody>
                </table>";
        
        return $tabl;
    }

    function type_duel($joueur_courant,$joueur1){
        if($joueur1 == $joueur_courant){
            return "attaque";
        }
        return "defense";
    }
    
    
    //prend en paramètre un objet duels issue de la Table Duels
    function etat_duels($joueur_courant,$duel){
        if($duel["archive"]){
            return "terminé";
        }
        return "en attente";
    }
    
    
    //retourne l'objet duel de la base de donnee
    function duel_defense($player_name){
        require_once $_SERVER["DOCUMENT_ROOT"]."/lab/connexion/connexion.php";
        $db = $GLOBALS["db"];
        
        $query = "select * from duels where joueur2 = :player and archive = false order by date_debut " ;
        $query_prepare = $db->prepare($query);
        $query_prepare->execute(Array(":player"=>$player_name));
        
        return $query_prepare->fetch(PDO::FETCH_ASSOC) ;
    }
    
    
    function verification_date_abandon($duel){
        
    }
    
    
    /*
     * 
     *          A REVOIRRRRRRRR
     * 
     * 
     */
    
    //a voir pour mettre cette fonction dans un autre fichier 
    // pour les points on ne s'occupe que de comparer les bonne reponse il n'y a pas "d'interet pedagogique" le details des reponses et des indications seront donne avec une autre fonction
    function compte_point($duel){
        require_once $_SERVER["DOCUMENT_ROOT"]."/lab/connexion/connexion.php";
        $db = $GLOBALS["db"];
        
        //la requete vas recupere pour chaque question contenue dans un duel les id de chaque choix bonne reponse
        $reponses_questions = "select choix.id as choix_id from Choix,questions where nom_question = nom AND nom_question in 
                                                                                                                                (select nom from questions where id in
                                                                                                                                   (select id_question from comporte where id_duel = :id_duel))";
        
        $query_prepare_reponse_question = $db->prepare($reponses_questions);
        $query_prepare_reponse_question->execute(Array(":id_duel"=>$duel["id"]));
        
        
        
        //on stocke les bonne reponse dans un tableaux
        $tableau_reponse;
        while($reponse = $query_prepare_reponse_question->fetch(PDO::FETCH_ASSOC)){
            $tableau_reponse[$reponse[choix_id]] = true;
        }
        
        
        //on selectione les choix de tout les joueurs pour un meme duel
        
        $reponse_joueur = "select joueur,id_choix from choix_joueur where id_duel = :id_duel order by joueur ";
        $query_prepare_reponse_joueur = $db->prepare($reponse_joueur);
        $query_prepare_reponse_joueur->execute(Array(":id_duel"=>$duel["id"]));
        
        //on stocke les score des deux joueurs dans le tableaux player_score
        $player_score = array(
            $duel["joueur1"] => 0,
            $duel["joueur2"] => 0
        );
        
        //on vas compter le nombre de bonne reponse pour chaque joueur
        while($reponse_question_query = $query_prepare_reponse_joueur->fetch(PDO::FETCH_ASSOC)){
            echo "<p>test pour savoir si il y a une ";
            //si il existe bien une reponse qui correspond entre ce qu'a choisi le joueur et ce qui est reel
            if($tableau_reponse[$reponse_question_query["id_choix"]]){
                //alors on incremente le score de 1 
                echo "<p>joueur : ".$reponse_question_query["joueur"]."</p>" ;
                $player_score[$reponse_question_query["joueur"]]++ ;
                echo "bonne reponse";
            }
            echo "</p>";
        }
        
        var_dump($player_score);
        
        //le score de chaque joueur est donne par la difference de bonne reponse avec son oposant , on peux revoir le systeme bien sur
        $gain_scoreJ1 = $player_score[$duel["joueur1"]] - $player_score[$duel["joueur2"]];
        $gain_scoreJ2 = $player_score[$duel["joueur2"]] - $player_score[$duel["joueur1"]];
        
        
        
        echo " gain du score : ".$player_score[$duel["joueur1"]]." : ".$gain_scoreJ1;
        echo " gain du score : j2 : ".$gain_scoreJ2;
        
        $duel["Points_j1"] += $gain_scoreJ1;
        $duel["Points_j2"] += $gain_scoreJ2;
        
        echo "<p>test : ".$gain_scoreJ1." ".$duel["joueur1"]."</p>";
        
        //on met a jour toute les table impacte
        $requete_ajout_de_score = "update players set score = score + :scoreJ1 where login = :J1";
        $requete_prepare = $db->prepare($requete_ajout_de_score);
        $requete_prepare->execute(Array(":scoreJ1"=>$gain_scoreJ1,":J1"=>$duel["joueur1"]));
        
        $requete_ajout_de_score = "update players set score = score + :scoreJ2 where login = :J2";
        $requete_prepare = $db->prepare($requete_ajout_de_score);
        $requete_prepare->execute(Array(":scoreJ2"=>$gain_scoreJ2,":J2"=>$duel["joueur2"]));
        
        $requete_ajout_score_duel = "update duels set points_j1 = :scoreJ1 where id = :id_duel AND joueur1 = :J1";
        $requete_prepare = $db->prepare($requete_ajout_score_duel);
        $requete_prepare->execute(Array(":scoreJ1"=>$gain_scoreJ1,":id_duel"=>$duel["id"],":J1"=>$duel["joueur1"]));
        
        $requete_ajout_score_duel = "update duels set points_j2 = :scoreJ2 where id = :id_duel AND joueur2 = :J2";
        $requete_prepare = $db->prepare($requete_ajout_score_duel);
        $requete_prepare->execute(Array(":scoreJ2"=>$gain_scoreJ2,":id_duel"=>$duel["id"],":J2"=>$duel["joueur2"]));
        
        $requete_close_calcul_point = "update duels set calcul_points = true where id = :id_duel";
        $requete_prepare = $db->prepare($requete_close_calcul_point);
        $requete_prepare->execute(Array(":id_duel"=>$duel["id"]));
        
        
        echo "mise a jour : ".$duel["Points_j1"];
        
        /* servira au reponse detaillee
        //le tableau reponse_questions vas synthetiser le total des reponse d'un duel sous la forme suivante : 
        /*
         * 
         * array (size=3)
              1 =>  <------ id de la question
                array (size=3)
                  2 => boolean true  <------ id de la reponse a la question => si c'est la bonne reponse ou pas
                  3 => boolean false
                  4 => boolean false
              3 => 
                array (size=3)
                  10 => boolean true
                  11 => boolean false
                  12 => boolean true
              4 => 
                array (size=4)
                  14 => boolean false
                  15 => boolean true
                  16 => boolean false
                  17 => boolean false
         *
        $reponse_questions = [] ;
        while($reponse_question_query = $query_prepare_reponse_question->fetch(PDO::FETCH_ASSOC)){
 
            $reponse_question[$reponse_question_query["question_id"]][$reponse_question_query["choix_id"]] = $reponse_question_query[solution];
            
        }
        var_dump($reponse_question);
        
        
        //le $reponse_joueur synthetise la reponse des joueurs on comparera la question des deux tableaux 
        /*
         * array (size=1)
              'tardi' =>  <---- le joueur tardi a fait les choix suivant 
                array (size=3)
                    3 => boolean true <--- choix de tardi 
                    13 => boolean true
                    11 => boolean true
                    
                    //dans ce tableau on peux voir que l'adversaire de tardi n'a repondue a aucune question
         * 
         *
        $reponse_joueur = [];
        while($reponse_joueur_query =  $query_prepare_reponse_joueur->fetch(PDO::FETCH_ASSOC)){
           
            $reponse_joueur[$reponse_joueur_query["joueur"]][$reponse_joueur_query["id_choix"]] = true;
           
        }
        var_dump($reponse_joueur);
        */
    }
    
    
?>

 